<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function index(){
        $packages = Package::all();
        return response()->json($packages);
    }
    public function store(Request $request)
    {
        Package::create([
            'adresse'=>$request->adresse,
            'adress'=>$request->adress,
            'nature'=>$request->nature,
            'tel'=>$request->tel,
            'price'=>$request->price,
            'state'=>$request->state,
            'city'=>$request->city,
            'packagee'=>$request->packagee,
            'comment'=>$request->comment
        ]);
        return response()->json('successfully created',201);
    }
    public function edit($id)
    {
        return response()->json(Package::whereId($id)->first());
    }
    public function update(Request $request, $id)
    {
        $user = Package::whereId($id)->first();

        $user->update([
            'adresse'=>$request->adresse,
            'adress'=>$request->adress,
            'nature'=>$request->nature,
            'tel'=>$request->tel,
            'price'=>$request->price,
            'state'=>$request->state,
            'city'=>$request->city,
            'packagee'=>$request->packagee,
            'comment'=>$request->comment
           
        ]);
        return response()->json('success');
    }
    public function destroy($id)
    {
        Package::whereId($id)->first()->delete();

        return response()->json('success');
    }
}
