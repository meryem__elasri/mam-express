import './App.css';
import { useEffect,useState} from 'react';
import Navbar from './components/User/Navbar';
import Home from './components/User/Home'
import Services from './components/User/Services';
import HowItWorks from './components/User/HowItWorks';
import Tarifs from './components/User/Tarifs';
import About from './components/User/About';
import {BrowserRouter,Routes,Route} from 'react-router-dom'
import Contact from './components/User/Contact';
import SignIn from './components/User/SignIn';
import SignUp from './components/User/SignUp';
import HomeClient from './components/Client/HomeClient';
import AddPackage from './components/Client/AddPackage';
import NavbarClient from './components/Client/NavbarClientVertical';
import ListPackage from './components/Client/ListPackage';
import PackageState from './components/Client/PackageState';
import DeliveryNotes from './components/Client/DeliveryNotes';
import listdeliverynotes from './components/Client/listdeliverynotes';
import Listdeliverynotes from './components/Client/listdeliverynotes';
import NotesRetour from './components/Client/NotesRetour';
import RequestRouter from './components/Client/RequestRouter';
import Factures from './components/Client/Factures';
import Settings from './components/Client/Settings';
// admin dashbord 
import Dashboard from './components/Admin/Dashboard';
import CreateInvoices from './components/Admin/CreateInvoices';
import Customers from './components/Admin/Customers';
import Invoices from './components/Admin/Invoices';
import Package from './components/Admin/Package';
import Pickup from './components/Admin/Pickup';
import Return from './components/Admin/Return';
import Sidebar from './components/Admin/SideBar';
import Header from './components/Admin/Header';

import './components/Admin/SideBar.css';


function App() {
  const [allServices, setAllServices] = useState([]);
  useEffect(() => {
    const FetchServ = async () => {
      const res = await fetch("Tarifs.json");
      const Tarifs = await res.json();
      setAllServices(Tarifs.Tarifs);
    };
    FetchServ();
  }, []);
  return (
    
    <BrowserRouter>
    <div className="App container-fuild">
     
     <Routes>
        {/* <Route path='/' element={<Home />}></Route>
        <Route path='services' element={<Services />}></Route>
        <Route path='/howitworks' element={<HowItWorks />}> </Route>
        <Route path='/pricing' element={<Tarifs allServices={allServices}/>}></Route>
        <Route path='/about' element={<About />}></Route>
        <Route path='/contact' element={<Contact />}></Route>
        <Route path='/signin' element={<SignIn />}></Route>
        <Route path='/signup' element={<SignUp />}></Route> */}
        <Route exact path="/" element={<div><Navbar /><Home /></div>} />
        <Route path="/about"  element={<div><Navbar /><About /></div>} />
        <Route path="/services" element={<div><Navbar /><Services /></div>} />
        <Route path="/howitworks"  element={<div><Navbar /><HowItWorks /></div>} />
        <Route path="/pricing"  element={<div><Navbar /><Tarifs allServices={allServices}/></div>} />
        <Route path="/contact"  element={<div><Navbar /><Contact /></div>} />
        <Route path="/signin"  element={<div><Navbar /><SignIn /></div>} />
        <Route path="/signup"  element={<div><Navbar /><SignUp /></div>} />
        <Route path="/homeclient" element={<HomeClient />}/>
        <Route path="/addpack" element={<AddPackage />}/>
        <Route path="/listpack" element={<ListPackage />}/>
        <Route path="/statepack" element={<PackageState />}/>
        <Route path="/deliverynotes" element={<DeliveryNotes />}/>
        <Route path="/listdeliverynotes" element={<Listdeliverynotes />}/>
        <Route path="/notesretour" element={<NotesRetour />}/>
        <Route path="/requestretour" element={<RequestRouter />}/>
        <Route path="/factures" element={<Factures />}/>
        <Route path="/settings" element={<Settings />}/>

      {/* admin dashboord */}
       {/* <Sidebar />
  <Header fixed="top"/> */}
      {/* <Route  path='/dashboard' element={<Sidebar />} />

      <Route  path='/dashboard' element={<Header fixed="top"/> } /> */}

      <Route  path='/dashboard' element={<Dashboard/>} />
    <Route  path='/customers' element={<Customers/>} />
    <Route path='/package' element={<Package/>} />
    <Route path='/invoices' element={<Invoices/>} />
    <Route path='/create' element={<CreateInvoices/>} />
    <Route path='/return' element={<Return/>} />
    <Route path='/pickup' element={<Pickup/>} />

      </Routes>   
    </div>
    </BrowserRouter>

   
    )
}

export default App;
