import React from 'react';
import NavbarClientVertical from './NavbarClientVertical';
import { faEye,  } from '@fortawesome/free-solid-svg-icons';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const PackageState = () => {
  return (
    <div className='ListPackage'> 
    <NavbarClientVertical/>
    <div className='bg w-100'>
    <div className='title_list'> 
    <h2 className='text-center'>Packages state</h2> 
    <a href='addpack'>
<button type='submit' className='btn btn-success m-4'>+ Add New </button>            </a>

    
    <div className='section_list_colis'>

    <table class="table table-striped ">
        <thead>
            <tr>
            <th scope="col">Id Package</th>
            <th scope="col">Addressee</th>
            <th scope="col">Tel</th>
            <th scope="col">Price</th>
            <th scope="col">Collection date</th>
            <th scope="col">State</th>
            <th scope="col">Status</th>
            <th scope="col">Delivery date</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Mark</td>
              <td>06053494455</td>
              <td>200 dh</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><FontAwesomeIcon icon={faEye} /> <FontAwesomeIcon icon={faWhatsapp} size="1x" color="green" />
</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>Jacob</td>
              <td>0616070682</td>
              <td>3000 dh</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><FontAwesomeIcon icon={faEye} /> <FontAwesomeIcon icon={faWhatsapp} size="1x" color="green" />
</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>Larry</td>
              <td>0605823465</td>
              <td>100 dh</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><FontAwesomeIcon icon={faEye} />     <FontAwesomeIcon icon={faWhatsapp} size="1x" color="green" />
</td>

            </tr>
        </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
  )
}

export default PackageState