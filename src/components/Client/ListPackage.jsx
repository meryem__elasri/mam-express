import React from 'react';
import './ListPackage.css';
import NavbarClientVertical from './NavbarClientVertical'
import { NavLink } from 'react-router-dom';

const ListPackage = () => {
return (
    <div className='ListPackage'> 
        <NavbarClientVertical/>
        <div className='bg w-100'>
        <div className='title_list'> 
        <h2 className='text-center'>List Packages</h2> 
        <a href='addpack'>
<button type='submit' className='btn btn-success m-4'>+ Add New </button>            </a>

        
        <div className='section_list_colis'>

        <table class="table table-striped ">
            <thead>
                <tr>
                <th scope="col">Id Package</th>
                <th scope="col">Addressee</th>
                <th scope="col">Tel</th>
                <th scope="col">City</th>
                <th scope="col">Adress</th>
                <th scope="col">Price</th>
                <th scope="col">Nature of product</th>
                <th scope="col">Comment</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>06053494455</td>
                <td>Agadir</td>
                <td>Quartiez tarrast</td>
                <td>200 dh</td>
                <td>Clothes</td>
                <td>Max date to deliver it is 30 march 2023</td>
                </tr>
                <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>0616070682</td>
                <td>Settat</td>
                <td>Boknal Quartiez</td>
                <td>3000 dh</td>
                <td>Electronics</td>
                <td></td>

                </tr>
                <tr>
                <th scope="row">3</th>
                <td>Larry</td>
                <td>0605823465</td>
                <td>Asfi</td>
                <td>souiria</td>
                <td>100 dh</td>
                <td>Clothes</td>
                <td></td>

                </tr>
            </tbody>
        </table>
        </div>

        </div>
        </div>
        
    </div>
    )
}

export default ListPackage