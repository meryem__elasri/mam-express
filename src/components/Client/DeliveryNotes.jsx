import React from 'react'
import './DeliveryNotes.css'
import NavbarClientVertical from './NavbarClientVertical'
const DeliveryNotes = () => {
    return (
        <div className='ListPackage'> 
        <NavbarClientVertical/>
        <div className='bg w-100'>
        <div className='title_list'> 
        <a href='addpack'>
<button type='submit' className='btn btn-success m-4'>+ Add New </button>            </a>

<h3 className='text-center'>List Packages</h3> 

        <div className='section_list_colis'>

        <table class="table table-striped ">
            <thead>
                <tr>
                <th scope="col">Id Package</th>
                <th scope="col">Addressee</th>
                <th scope="col">Tel</th>
                <th scope="col">City</th>
                <th scope="col">Adress</th>
                <th scope="col">City</th>
                <th scope="col">Price</th>
                <th scope="col">Nature of product</th>
                <th scope="col">Add note</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>@mdo</td>
                <td>@mdo</td>
                <td>@mdo</td>
                <td>@mdo</td>
                <td><input type='checkbox'/></td>
                </tr>
                <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td>Thornton</td>
                <td>Thornton</td>
                <td>Thornton</td>
                <td>Thornton</td>
                <td><input type='checkbox'/></td>

                </tr>
                <tr>
                <th scope="row">3</th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
                <td>Thornton</td>
                <td>Thornton</td>
                <td>Thornton</td>
                <td>Thornton</td>
                <td><input type='checkbox'/></td>

                </tr>
            </tbody>

        </table>
        <div className='bg_grey'>
        <button className='btn btn-success float-end mt-1' type='submit'>Add +</button>


        </div>

        </div>

        </div>
        <h3 className='text-center'>List Added packages</h3> 
        <div className='section_list_colis'>

<table class="table table-striped ">
    <thead>
        <tr>
        <th scope="col">Id Package</th>
        <th scope="col">Addressee</th>
        <th scope="col">Tel</th>
        <th scope="col">City</th>
        <th scope="col">Adress</th>
        <th scope="col">City</th>
        <th scope="col">Price</th>
        <th scope="col">Nature of product</th>
        <th scope="col">Remove note</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
        <td>@mdo</td>
        <td>@mdo</td>
        <td>@mdo</td>
        <td>@mdo</td>
        <td>@mdo</td>
        <td><input type='checkbox'/></td>
        </tr>
        <tr>
        <th scope="row">2</th>
        <td>Jacob</td>
        <td>Thornton</td>
        <td>@fat</td>
        <td>Thornton</td>
        <td>Thornton</td>
        <td>Thornton</td>
        <td>Thornton</td>
        <td><input type='checkbox'/></td>

        </tr>
        <tr>
        <th scope="row">3</th>
        <td>Larry</td>
        <td>the Bird</td>
        <td>@twitter</td>
        <td>Thornton</td>
        <td>Thornton</td>
        <td>Thornton</td>
        <td>Thornton</td>
        <td><input type='checkbox'/></td>

        </tr>
    </tbody>

</table>
<div className='bg_grey'>
        <button className='btn btn-success float-end mt-1' type='submit'>Remove -</button>


        </div>

        </div>
        </div>
        </div>

    )
}

export default DeliveryNotes