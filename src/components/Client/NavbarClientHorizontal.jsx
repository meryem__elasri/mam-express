import React from 'react'
import './NavbarClient.css'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMoon,faSun } from '@fortawesome/free-solid-svg-icons'

import Navbar from 'react-bootstrap/Navbar';
import Image from '../../images/Beige Aesthetic Simple Elegant Photo iOS Icons (3).png'
const NavbarClientHorizontal = () => {
  return (
    <div class="content">
        <nav>
        <Navbar className='bg-blue horizentalNav' expand="lg">
    <Container>
        <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="">
            <Nav.Link href="homeclient" className='text-light font-weight-bold p-lg-3'>Home</Nav.Link> 
            <Nav.Link  className='text-light font-weight-bold p-lg-3'><FontAwesomeIcon icon={faMoon} /></Nav.Link> 
            <Nav.Link  className='text-light font-weight-bold p-lg-3'><FontAwesomeIcon icon={faSun} /></Nav.Link> 
            <Nav.Link  className='text-light font-weight-bold p-lg-3'>Languages</Nav.Link> 


            <Nav.Link href="settings" className='text-light'><img className='icon_navigation_horezental' src={Image} alt='erreur'/></Nav.Link> 
        </Nav>      
        </Navbar.Collapse>
    </Container>
    </Navbar>
        </nav>
    </div> 
  )
}

export default NavbarClientHorizontal