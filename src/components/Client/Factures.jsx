import React from 'react'
import NavbarClientVertical from './NavbarClientVertical';
import { faEye  } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Factures = () => {
  return (
<div className='ListPackage'> 
    <NavbarClientVertical/>
    <div className='bg w-100'>
    <div className='title_list'> 
    <h2 className='text-center'>List bill</h2> 
  

<div className='section_list_colis'>

    <div className='rechercher float-end'>
    <label >Search : </label>
    <input type='search'/>
    </div>
        <table class="table table-striped ">
        <thead>
            <tr>
            <th scope="col">ID </th>
            <th scope="col">Date Payment</th>
            <th scope="col">Amount</th>
            <th scope="col">nbr Colis</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td></td>
            <td></td>
            <td></td>
            <td><FontAwesomeIcon icon={faEye} /></td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td></td>
            <td></td>
            <td></td>
            <td><FontAwesomeIcon icon={faEye} /></td>


            </tr>
            <tr>
            <th scope="row">3</th>
            <td></td>
            <td></td>
            <td></td>
            <td><FontAwesomeIcon icon={faEye} /></td>
            </tr>
        </tbody>
    </table>
    </div>

    </div>
    </div>
    </div>  )
}

export default Factures