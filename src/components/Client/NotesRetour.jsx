import React from 'react'
import NavbarClientVertical from './NavbarClientVertical';
import { faEye, faDownload  } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
const NotesRetour = () => {
  return (
    <div className='ReturnedRequest '> 
    <NavbarClientVertical/>
    <div className='ReturnedRequest_bg'>
    <h3 className='m-5 '>Pickup Request  </h3>

    <div className='request_info'>
    <label className='mb-2'>Where do you want us to meet you to receive your packages ? </label> <br/>
    <select value='Select your city' className='form-control w-50 '>
    <option>Agadir</option>
    <option>Safi</option>
    <option>Safi</option>
    <option>Safi</option>
    <option>Safi</option>
    </select><br/>
    <label className='mb-2'>nbr packages : </label> 
    <input className="form-control mr-sm-2" type="number"/>
    <label className='mb-2'>do you want to say something else : </label> <br/>
    <textarea className='w-50 h-25 mb-5'  />  <br/>
    <button className='btn btn-success'>Send Request</button>
    </div>
    </div>

    </div>

  )
}

export default NotesRetour;