import React from 'react'
import './AddPackage.css'
import NavbarClientVertical from './NavbarClientVertical'
const AddPackage = () => {
  return (
    <div className='AddPackage_NavbarClientVertical'>
    <NavbarClientVertical />
    <div className='AddPackage'>
    

    <form>
    <h1 className='text-center'>New package</h1>
    <div class="states">
    <div class="col-md-4 form_add">
      <label for="inputState">Package</label>
      <select id="inputState" class="form-control">
        <option>Open package</option>
        <option>Don't open package</option>
      </select>
    </div>
    <div class=" col-md-4 form_add">
      <label for="inputState">State</label>
      <select id="inputState" class="form-control">
        <option selected>Solid  </option>
        <option>liquid </option>
        <option> glass</option>

      </select>
    </div>
    </div>
    <div className='states'>
    <div class=" col-md-4 form_add">
      <label for="inputEmail4">Addressee</label>
      <input type="text" class="form-control" id="inputEmail4" placeholder="Name"/>
    </div>
 
  <div class="col-md-4 form_add">
    <label for="inputAddress">Tel</label>
    <input type="tel" class="form-control" id="inputAddress" placeholder="+212 000000000"/>
  </div>
  <div class="col-md-3 form_add">
    <label for="inputAddress2">City</label>
    <input type="text" class="form-control" id="inputAddress2" placeholder="city"/>
  </div>
  </div>
  <div className='states'>
    <div class=" col-md-4 form_add">
      <label for="inputCity">Adress</label>
      <input type="text" class="form-control" id="inputCity" placeholder='Neighborhood ,House Number'/>
    </div>
    
    <div class=" col-md-4 form_add">
      <label for="inputZip">Price</label>
      <input type="number" class="form-control" id="inputZip"/>
    </div>
    </div>
    <div class="states">
    <div class=" col-md-4 form_add">
      <label for="inputCity">Nature of product</label>
      <input type="text" class="form-control" id="inputCity" placeholder='Clothes,Electronics...'/>
    
  </div>
  <div class=" col-md-4 form_add">
      <label for="inputCity">Comment</label>
      <textarea type="textarea" class="form-control" id="inputCity" placeholder='other tel, Delivery date'/>
    
  </div>
  </div>
  <button type="submit" class="btn btn-primary">Record</button>
</form>
</div>
</div>
  )
}

export default AddPackage