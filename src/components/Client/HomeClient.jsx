import React from 'react';
import './HomeClient.css';
import NavbarClientVertical from './NavbarClientVertical';
import NavbarClientHorizontal from './NavbarClientHorizontal'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck ,faDollarSign ,faSpinner, faFileExport  } from '@fortawesome/free-solid-svg-icons';
const HomeClient = () => {
    return (
        <div className='Home_NavbarClientVertical '>
                <NavbarClientVertical />
                <div className='Home_client'>
                <NavbarClientHorizontal />
            <span><h1 className='text-center'>Statistics</h1></span>
            <div class="card_homeclient">
                <div class="box w-50">
                <div><h1>Income</h1><div className='span_icons_green'><FontAwesomeIcon icon={faDollarSign} color='white' size="2x" className='icons_statistics' /></div></div>
                </div>
                <div class="box">
                <div><h6>in progress </h6><div className='span_icons_green'>
                <FontAwesomeIcon icon={faSpinner} color='white' size="2x" className='icons_statistics' /></div></div>
                </div>
                <div class="box">
                <div><h6> delivered invoiced</h6><div className='span_icons_green'><FontAwesomeIcon icon={faCheck} color='white' size="2x" className='icons_statistics' /></div></div>
                </div>
                <div class="box">
                <div>
                        
                        
                        <h6> delivered not invoiced</h6>
                        <div className='span_icons_red'>
                        <FontAwesomeIcon icon={faCheck} color='white' className='icons_statistics'size="2x" />
                    </div>
                    </div>
                </div>
                <div class="box">
                   <div><h6>Returned</h6><div className='span_icons_red'><FontAwesomeIcon icon={faFileExport} color='white' size="2x" className='icons_statistics'/></div></div>
                </div>
            </div> 
        </div>
        </div>
    )
}

export default HomeClient
