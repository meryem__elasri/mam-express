import React from 'react'
import './NavbarClient.css'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome,faBoxes,faFile,faCog,faFolder,faBox,faReply ,faFileInvoice,faFileExport,faChartBar} from '@fortawesome/free-solid-svg-icons'
import Image  from '../../images/Beige Aesthetic Simple Elegant Photo iOS Icons (3).png'

const NavbarClientVertical = () => {
    return (
    <>
    <div class="section_client">
    <div class="vertical_navbar">
        <div>
            <h1 className='logo_mamexpress'>MAMexpress</h1>
            <a href='settings'> <img src={Image} className='userlogo' alt='erreur'/></a>
        </div>
        <ul>
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faHome} color='#393a6d' />
                <a href="/homeclient"  className='icon_menu_vertical'>Home</a>
            </li>
            <li class="list_navbar_vertical ">
            <FontAwesomeIcon icon={faBox} color='#393a6d' />

                <a href="/addpack" className='icon_menu_vertical'>
                New Package
                </a>
            </li>
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faBoxes} color='#393a6d' />

                <a href="listpack" className='icon_menu_vertical'>
                List Package
                </a>
            </li>
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faChartBar } color='#393a6d' />
                <a href="statepack" className='icon_menu_vertical'>
                Package state              
                </a>
            </li>
          
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faFolder} color='#393a6d' />
                <a href="listdeliverynotes" className='icon_menu_vertical'>
                     Delivery notes
                </a>
            </li>
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faFileExport} color='#393a6d' />
                <a href="notesretour" className='icon_menu_vertical'>
                    Pickup Request
                </a>
            </li>
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faReply  } color='#393a6d' />
                <a href="requestretour" className='icon_menu_vertical'>
                    request retour
                </a>
            </li>
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faFileInvoice  } color='#393a6d' />

                <a href="factures" className='icon_menu_vertical'>
                    Facture
                </a>
            </li>
            <li class="list_navbar_vertical">
            <FontAwesomeIcon icon={faCog  } color='#393a6d' />

                <a href="settings" className='icon_menu_vertical'>
                    Settings
                </a>
            </li>
        </ul> 
        </div>
       
</div>
</>
)
}

export default NavbarClientVertical