import React from 'react'
import NavbarClientVertical from './NavbarClientVertical';
import { faEye, faDownload  } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const Listdeliverynotes = () => {
  return (
    <div className='ListPackage'> 
    <NavbarClientVertical/>
    <div className='bg w-100'>
    <div className='title_list'> 
    <h2 className='text-center'>List Delivery Notes</h2> 
    <a href='deliverynotes'>
<button type='submit' className='btn btn-success m-4'>+ Add New </button>            </a>

<div className='section_list_colis'>

    <div className='rechercher float-end'>
    <label >Search : </label>
    <input type='search'/>
    </div>
        <table class="table table-striped ">
        <thead>
            <tr>
            <th scope="col">Ref Note</th>
            <th scope="col">Date create </th>
            <th scope="col">Colis</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>1</td>
            <td><FontAwesomeIcon icon={faEye} /><FontAwesomeIcon icon={faDownload} /></td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>1</td>
            <td><FontAwesomeIcon icon={faEye} />  <FontAwesomeIcon icon={faDownload} /></td>
           

            </tr>
            <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>1</td>
            <td><FontAwesomeIcon icon={faEye} /> <FontAwesomeIcon icon={faDownload} /></td>
      

            </tr>
        </tbody>
    </table>
    </div>

    </div>
    </div>
    </div>

  )
}

export default Listdeliverynotes;