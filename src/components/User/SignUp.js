import React from 'react'
import { Link } from 'react-router-dom';
import './SignUp.css';

export default function SignUp(props) {
  return (
    
        <div className="Auth-form-container">
          <form className="Auth-form">
            <div className="Auth-form-content">
              <h3 className="Auth-form-title">Sign Up</h3>
              <div className="text-center">
                Already registered?{" "}
                <Link to='/signIn' className="link-primary"> Sign In </Link>
              </div>
              <div className="form-group mt-3">
                <label>Full Name</label>
                <input type="text" className="form-control " placeholder="Full Name"
                />
              </div>
              <div className="form-group mt-3">
                <label>City</label>
                <input type="text" className="form-control " placeholder="City"/>
              </div>
              <div className="form-group mt-3">
                <label>Phone</label>
                <input type="Number" className="form-control " placeholder="Phone"/>
              </div>
              <div className="form-group mt-3">
                <label>Email address</label>
                <input type="email" className="form-control" placeholder="Email Address"/>
              </div>
              <div className="form-group mt-3">
                <label>Password</label>
                <input type="password" className="form-control " placeholder="Password"/>
              </div>
              <div className="d-grid gap-2 mt-3">
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </div>
              <p className="text-center mt-2">
                Forgot <a href="#">password?</a>
              </p>
            </div>
          </form>
        </div>
      )
    }
  

