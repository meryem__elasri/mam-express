import React,{useState,useNavigate} from "react";
import './Tarifs.css';

const Tarifs = ({allServices}) => {
    const [search, setSearch]= useState('');
    // const navigate = useNavigate();
    return (
    <div className="container">
        <div className="section_tarif">
        <h1 className="text-center title_tarif">Pricing</h1>
        <input type='text' placeholder='search city...'  onChange={(e)=>setSearch(e.target.value)} className="form-control btn_search "></input> 
        <table class="table table-bordered">
        <thead>
        <tr class="table-color">
            <th>Ref</th>
            <th>City</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>
        {allServices.filter((tar)=>{
        return search.toLowerCase()==='' ? tar:tar.code.toLowerCase().includes(search);}).map((tarif) => {
        return (
            <tr key={tarif.code} className="row-link" >
                <td>{tarif.code}</td>
                <td>{tarif.ville}</td>
                <td>{tarif.tarifs}</td>
            </tr>);
        })}
        </tbody>
    </table>
    </div>  
    </div>

)
}

export default Tarifs
