import React from 'react'
import { Link } from 'react-router-dom';
import Image from '../../images/Orange Minimalist Logistics and Moving Services Logo (1).png'
import './About.css'
const About = () => {
  return (
  <div className='About_page'>
      <h1 className='title_about'>About Us</h1>
      <div class="about_info">
      <img src={Image}  alt="doesn't exist"/>
      <div class="about-section">
      <p className='h4'>We are <span className='span_about'> MAMexpress</span></p>
      <p className='h5 '>Serving you and helping you succeed is our priority</p>
      <p className='p-1'>For years, we have been studying the problems of delivery in Morocco and trying to develop practical solutions to all the problems faced by sellers, such as delayed deliveries, loss of packages, high prices for delivery, as well as delayed return of rejected packages, delayed receipt of profits and many other problems... This is what helped us in our success by virtue of understanding customer requirements and working with the principle of 'win-win' .</p>
      <p className='encoraged '>If you want to succeed in your trade join us!</p>
      <Link to='/signin'><button className='btn ' id='btn-color'>Join Us</button></Link>
    </div>
    </div>
  </div>
  )
}

export default About
