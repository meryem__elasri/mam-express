import './Navbar.css';
// import { NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
        
    

function Navbars() {
  return (
    <Navbar className='bg-blue container_navbar' expand="lg">
      <Container>
        <Navbar.Brand href="#home"><span className='logo'>MAM express</span></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/" className='text-light font-weight-bold p-lg-3'>Home</Nav.Link>
            <Nav.Link href="/about" className='text-light font-weight-bold p-lg-3'>About</Nav.Link>
            <Nav.Link href="/services" className='text-light font-weight-bold p-lg-3'>Services</Nav.Link>
            <Nav.Link href="/howitworks" className='text-light font-weight-bold p-lg-3'>HowItWorks</Nav.Link>
            <Nav.Link href="/pricing" className='text-light font-weight-bold p-lg-3'>Pricing</Nav.Link>
            <Nav.Link href="/contact" className='text-light font-weight-bold p-lg-3'>Contact</Nav.Link>
          </Nav>
          <Nav.Link href="/signin" className='btnnav text-light font-weight-bold p-lg-3'>Sign In</Nav.Link>
          <Nav.Link href="/signup" className='btnnav text-light font-weight-bold p-lg-3'>Sign Up</Nav.Link>
          
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}



export default Navbars;
