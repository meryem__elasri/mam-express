import './Services.css';


function Services (){
    return(
        <section className='services'>
            <div className='services_pretty'>
        <h1 className='title_services'>Our Services</h1>
            <div className='services_all_cards'>
            
                <div  className='services_cards'>
                <div className='icons'>  
                <i class="fa-sharp fa-solid fa-boxes-stacked services_icon"></i>  
                </div>
                <div className='info'> 
                <h4  className='title_card'>Collection</h4>
                <p>One of our agents will<br></br> come to your home to pick up your packages.</p>
                </div>
                </div>
                <div  className='services_cards'>
                <div className='icons'>  
                <i class="fa-sharp fa-solid fa-boxes-packing services_icon"></i>
                </div>
                <div className='info'> 
                <h4  className='title_card'>Packaging</h4>
                <p>We provide attractive packaging<br></br> for your products that contributes to raising the delivery rate .</p>
                </div>
                </div>
                <div className='services_cards'>
                <div className='icons'> 
                <i class="fa-solid fa-truck services_icon"></i>   
                </div>      
                <div className='info'> 
                    <h4 className='title_card'>Delivery</h4>
                <p>We deliver your packages<br></br>
                    as soon as possible While preserving it,
                    with reasonable prices.
                </p>
                </div>
                </div>
                <div className='services_cards '>
                <div className='icons'> 
                <i class="fa-solid fa-right-left services_icon"></i>               
                </div> 
                <div className='info'> 
                <h4 className='title_card'>Aftercare</h4>
                <p>Tracking the status of<br></br> your packages (delivered, returned, paid, invoiced).</p>
                </div>
                </div>
            </div>
            </div>
        </section>
    )
}
export default Services;