import React from 'react';
import './HowItWorks.css';
import Image1 from '../../images/1.png';
import Image2 from '../../images/2.png';
import Image3 from '../../images/3.png';
import Image4 from '../../images/4.png';
import Image5 from '../../images/5.png';
import Image6 from '../../images/6.png';
import Carousel from 'react-bootstrap/Carousel';

function HowItWorks() {
  return (
    <div className='HowItWorks'>
    <h1 className='text-center pt-2 title-work'>How It Works</h1>
    <Carousel>
      <Carousel.Item> <img className=" image-slider" src={Image1} alt="First slide"/> 
        <Carousel.Caption>
          <h1 className='h1_slider'>Add your packages</h1>
          <p className='Info_slider'>Add your packages at anytime and in anywhere you want, and download the delivery notes .</p>
          <p className='h4'>Swipe right</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item> <img className="image-slider" src={Image2} alt="Second slide"/> 
        <Carousel.Caption>
          <h1 className='slider_type2'>request collection request</h1>
          <p className='Info_slider_t2'>Or deliver it to our company's branche closest to you.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item> <img className="image-slider" src={Image3} alt="Third slide"/> 
        <Carousel.Caption>
          <h1 className='slider_type3'>Delivery</h1>
          <p className='Info_slider_t3'>We will deliver it to the customer as soon as possible .</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item> <img className="image-slider" src={Image4} alt="Fourd slide"/> 
        <Carousel.Caption>
          <h1 className='h1_slider'>Tracking  packages</h1>
          <p className='Info_slider'>Tracking the status of your packages :
Was it delivred or was it rejected, what is the reason for its rejection, or is it still in the process of delivery?.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item> <img className="image-slider" src={Image5} alt="five slide"/>
        <Carousel.Caption>
          <h1 className='slider_type2'>Return request</h1>
          <p className='Info_slider_t2'>If you have returned packages make  request of refund your packages,and we will deliver it to you as soon as possible or you can  take it from the nearest headquarters. .</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item> <img className=" image-slider" src={Image6} alt="six slide"/>
        <Carousel.Caption>
          <h1 className='h1_slider'>Receive your money</h1>
          <p className='Info_slider'>We will send the money to your bank account or you can receive it in cash at one of our branches .</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    </div>
  );
}

export default HowItWorks;