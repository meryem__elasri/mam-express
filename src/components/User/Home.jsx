import  {useEffect,useState} from 'react';
import './Home.css';
import Services from './Services';
import HowItWorks from './HowItWorks';
import Contact from './Contact';
import Tarifs from './Tarifs'
import {Link} from 'react-router-dom'
import About from './About';
// import { useState,useEffect } from 'react';
function Home(){
    const [allServices, setAllServices] = useState([]);
    useEffect(() => {
      const FetchServ = async () => {
        const res = await fetch("Tarifs.json");
        const Tarifs = await res.json();
        setAllServices(Tarifs.Tarifs);
      };
      FetchServ();
    }, []);
    return(
        <>
        <section className='Home_page'>  
        <div className='home'>
        <div class="hero-page">
            <h1 > Your  <span className='span'> Partner</span> To Success</h1>
            <p className='paragraph' >We provide fast delivery in 24 hours <br/>  while  preserving your products and<br/>  dealing 
            well with customers</p>
            <Link to='/signin'><button className='btn ' id='btn-color'>Join Us</button></Link>
        </div>
        </div>
        </section>  

        <Services /> 
        <About />
        <HowItWorks />  
        <Tarifs allServices={allServices}/> 
        <Contact /> 
        </>

    )
}
export default Home;
