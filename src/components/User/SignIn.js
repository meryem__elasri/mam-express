
import React from "react"
import { Link } from "react-router-dom";
import './SignIn.css';
export default function SignIn (props) {
  // let [authMode, setAuthMode] = useState("signin")

  // const changeAuthMode = () => {
  //   setAuthMode(authMode === "signin" ? "signup" : "signin")
  // }

  // if (authMode === "signin") {
    return (
      <div className="Auth-form-container">
        <form className="Auth-form">
          <div className="Auth-form-content">
            <h3 className="Auth-form-title">Sign In</h3>
            <div className="text-center">
              Not registered yet?{" "}
              {/* <span className="link-primary" onClick={changeAuthMode}> */}
              <Link to='/signUp' className="link-primary"> Sign Up </Link>
               
              {/* </span> */}
            </div>
            <div className="form-group mt-3">
              <label>Email address</label>
              <input
                type="email"
                className="form-control mt-1"
                placeholder="Enter email"
              />
            </div>
            <div className="form-group mt-3">
              <label>Password</label>
              <input
                type="password"
                className="form-control mt-1"
                placeholder="Enter password"
              />
            </div>
            <div className="d-grid gap-2 mt-3">
              <Link to='/homeclient'>
                <button type="submit" className="btn btn-primary">
                Submit
              </button>
              </Link>
            </div>
            <p className="text-center mt-2">
              Forgot <a href="#l">password?</a>
            </p>
          </div>
        </form>
      </div>
    )
  }
   
