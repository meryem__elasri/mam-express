import Sidebar from './SideBar';
import Header from './Header';
import './SideBar.css';

export default function CreateInvoices(){
    return (

        <>
        <header className="fixed">
        <Header />
      </header>
      <div className="sidebar fixed">
     < Sidebar />
      </div>
<div className="main">
  
 
  <div className="card mb-4">
    <div className="card-header">Create Invoices</div>
    <div className="card-body">
      <form method="POST" action="" encType="multipart/form-data">
          <div className="col">
            <div className="mb-3 row">
              <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
               
                Customer Name:
              </label>
              <div className="col-lg-10 col-md-6 col-sm-12">
                <input
                  name="cname"
                 
                  type="text"
                  className="form-control"
                />
              </div>
            </div>
          </div>
          <div className="col">
            <div className="mb-3 row">
              <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                
                Amount:
              </label>
              <div className="col-lg-10 col-md-6 col-sm-12">
                <input
                  name="amount"
                  defaultValue="{{ old('amount') }}"
                  type="number"
                  className="form-control"
                />
              </div>
            </div>
          </div>
        <div className="row">
          <div className="col">
            <div className="mb-3 row">
              <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                Payment date:
              </label>
              <div className="col-lg-10 col-md-6 col-sm-12">
                <input
                  name="Paymentdate"
                  defaultValue="{{ old('Paymentdate') }}"
                  type="date"
                  className="form-control"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <div className="mb-3 row">
              <label className="col-lg-2 col-md-6 col-sm-12 col-form-label">
                Nbr packages:
              </label>
              <div className="col-lg-10 col-md-6 col-sm-12">
                <input
                  name="nbrpackages"
                  defaultValue="{{ old('nbrpackages') }}"
                  type="number"
                  className="form-control"
                />
              </div>
            </div>
          </div>
        </div>
     
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  </div>
  <div>
    </div>
  </div>
</>

    )}