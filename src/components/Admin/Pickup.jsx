import Sidebar from './SideBar';
import Header from './Header';
import './SideBar.css';
export default function Pickup(){
return(
  <>
  <header className="fixed">
  <Header />
</header>
<div className="sidebar fixed">
< Sidebar />
</div>
<div className="main">
    
    <div className="card mb-4">
<div className="card-body">
<div className="card p-5">
  <div className="card-header">Pickup Requests</div>
  <div className="row">
    <div className="col-md-4 col-lg-3 mb-2"></div>
  </div>
  <div className="card-body">
    <div></div>
    <table className="table table-bordered table-striped ">
      <thead>
        <tr>
          <th scope="col">ID</th>
          <th scope="col">Client</th>
          <th scope="col">Pickup's City</th>
          <th scope="col">nbr Packages</th>
          <th scope="col">Client message</th>
        
        </tr>
      </thead>
      <tbody className="bg-success"></tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>
</>
)
}
