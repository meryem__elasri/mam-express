import Sidebar from './SideBar';
import Header from './Header';
import './SideBar.css';
export default function Dashboard(){
    return (
      <>
      <header className="fixed">
      <Header  />
    </header>
    <div className="sidebar fixed">
   < Sidebar />
    </div>
<div className="main">

  <div className="cardBox ">
    <div className="card">
      <div>
        <div className="numbers">1,504</div>
        <div className="cardName">Our clients</div>
      </div>
      <div className="iconBx">
        <ion-icon name="eye-outline" />
      </div>
    </div>
    <div className="card">
      <div>
        <div className="numbers">80</div>
        <div className="cardName">Daily Packages</div>
      </div>
      <div className="iconBx">
        <ion-icon name="cart-outline" />
      </div>
    </div>
    <div className="card">
      <div>
        <div className="numbers">284</div>
        <div className="cardName">Comments</div>
      </div>
      <div className="iconBx">
        <ion-icon name="chatbubbles-outline" />
      </div>
    </div>
    <div className="card">
      <div>
        <div className="numbers">$7,842</div>
        <div className="cardName">Daily earning</div>
      </div>
      <div className="iconBx">
        <ion-icon name="cash-outline" />
      </div>
    </div>
  </div>
  </div>


  </>


    )
}