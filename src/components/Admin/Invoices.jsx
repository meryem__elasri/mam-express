import Sidebar from './SideBar';
import Header from './Header';
import './SideBar.css';
export default function Invoices(){
return(
  <>
  <header className="fixed">
  <Header />
</header>
<div className="sidebar fixed">
< Sidebar />
</div>

<div className="main">
    
  <div className="row"></div>
  <div className="dropdown-divider border-warning" />
  <div className="card-header row ml-4 ">
 
    <div className="col-md-6">
      <h5 className="fw-bold mb-0">List of Invoices</h5>
    </div>
    <div className="col-md-6">
      <div className="d-flex justify-content-end">
        <a
          href="/process.php?action=export"
          className="btn btn-success btn-sm"
          id="export"
        >
          <i className="fas fa-table" /> Exporter
        </a>
      </div>
    </div>
  </div>
  <div className="dropdown-divider border-warning" />
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-md-12">
        <div className="card">
          <div className="card-body">
            <div className="button">
              <a href="/create" className="btn btn-sm btn-primary">
                Create New Invoices
              </a>
            </div>
            <br />
            <form method="post" action="#" className="form-inline my-2 my-lg-0">
              <input
                className="form-control mr-sm-2"
                type="text"
                placeholder="Search"
                aria-label="Search"
                style={{ float: "right" }}
              />
              <button
                className="btn btn-outline-success my-2 my-sm-0"
                type="submit"
                style={{ float: "right" }}
              >
                Search
              </button>
            </form>
           
            <table className="table table-responsive-sm table-bordered table-striped">
              <thead>
                <tr>
                  <th>Invoice Number</th>
                  <th>Customer Name</th>
                  <th>phone</th>
                  <th>Total</th>
                  <th>Date create</th>
                  <th>City</th>
                  <th>quantity</th>
                  <th>state</th>
                  <th>edit</th>
                  <th>delete</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>h</td>
                  <td>h</td>
                  <td>h</td>
                  <td>h</td>
                  <td>h</td>
                  <td>h</td>
                  <td>h</td>
                  <td>h
                    {/* <select className="form-select" id="state" name="state">
                      <option
                        value="invoiced
                                      "
                      >
                        Invoiced
                      </option>
                      <option value="pay">pay</option>
                      <option value="canceled">Canceled</option>
                    </select> */}
                  </td>
                  <td className="px-4 py-3">
                    <a href="javascript:;" className="">
                    <ion-icon name="create-outline"/>
                    </a>
                  </td>
                  <td className="px-4 py-3">
                    <a href="javascript:;" className="">
                    <ion-icon name="trash-outline"/>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</>
)

}
