import Sidebar from './SideBar';
import Header from './Header';
import './SideBar.css';
export default function Package(){
    return ( <>
      <header className="fixed">
      <Header />
    </header>
    <div className="sidebar fixed">
   < Sidebar />
    </div>
    
<div className="main">
        
    <div className="page-wrapper">
  <div className="page-content">
    <div className="card">
      <div className="card-body">
        <div className="d-lg-flex align-items-center mb-4 gap-3">
          <form method="post" action="#" className="form-inline my-2 my-lg-0">
            <input
              className="form-control mr-sm-2"
              type="text"
              placeholder="Search"
              aria-label="Search"
              style={{ float: "right" }}
            />
            <button
              className="btn btn-outline-success my-2 my-sm-0"
              type="submit"
              style={{ float: "right" }}
            >
              Search
            </button>
          </form>
        </div>
        <div className="table-responsive">
          <table className="table mb-0">
            <thead className="table-light">
              <tr>
              <th>Client's Name </th>
                <th>Id Package</th>
                <th> Name</th>
                <th> Phone</th>
                <th>Status</th>
                <th>State</th>
                <th>City</th>
                <th>Adress</th>
                <th>Total</th>
                <th>Date</th>
                <th>Nature of product</th>
                <th>Collection date</th>
                <th>Delivery Status  </th>
                <th>Payment Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
              <td>Nour</td>
                <td>
                  <div className="d-flex align-items-center">
                   
                      <h6 className="mb-0 font-14">#OS-000354</h6>
                  </div>
                </td>
                <td>Gaspur Antunes</td>
                <td>09876542</td>
                <td>Don't open package</td>
                <td>solid</td>
                <td>Agadir</td>
                <td> morocco afadidko</td>
                <td>$485.20</td>
                <td>June 10, 2020</td>
                <td>Cloths</td>
                <td>Not yet done  <a href="javascript:;" className="ms-3">
                      <i className="fas fa-edit" />
                    </a></td> 
                <td>In progress  <a href="javascript:;" className="ms-3">
                      <i className="fas fa-edit" />
                    </a></td>
                <td>Not yet  <a href="javascript:;" className="ms-3">
                      <i className="fas fa-edit" />
                    </a></td> 
                <td>
                  <div className="d-flex  order-actions">
                    <a href="javascript:;" className="ms-3">
                      <i className="fas fa-edit" />
                    </a>
                    <a href="javascript:;" className="pl-3">
                      <i className="fas fa-trash" />
                    </a>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</>
    )
    

}
